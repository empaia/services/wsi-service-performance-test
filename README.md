# WSI File Format and Performance Test

Evaluate WSI readers against a range of WSIs. Check format consistency and performance.

## How to run the tests

Make sure docker, poetry and openslide are installed. Run once:

```shell
poetry install
```

Run tests with:

```shell
poetry run tests
```

The results are written to `data/results`. Every WSI reader is tested with every WSI.

To check out the results there is a simple html page which can be served with:

```shell
python3 -m http.server 8000
```

Afterwards visit `http://localhost:8000` to see an overview of all results.

## Compare against current results

Run comparison by running the compare script after executing the tests:

```shell
poetry run tests
poetry run compare
```

The script will fail if a result has changed from True to False or if the performance has decreased more than 20%.

## How to add a new WSI reader

Create a new WSI reader by creating a new `reader_NAME.py` file within the `reader` folder and implementing a new reader class that inherits a reader template (`SlideReader` or `SlideReaderAsync`), e.g.

```python
from wsi_file_format_and_performance_test.reader_template import SlideReader


class SlideReaderNewClass(SlideReader):
    ...
```

## How to add a new WSI

Add a new json to the `data` folder following an existing WSI, e.g. `cmu-1-svs.json`. Each file must fulfill the SlideFile model in the `models.py`.
