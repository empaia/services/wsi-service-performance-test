import glob
import json
import os
import pathlib

from wsi_file_format_and_performance_test.helpers import (
    get_data_path,
    get_reader_classes,
)
from wsi_file_format_and_performance_test.models import SlideFile
from wsi_file_format_and_performance_test.tests import (
    test_performance,
    test_slide_data,
    test_slide_info,
)


def create_index():
    slide_file_list = []
    slide_reader_list = []
    filepaths = glob.glob(os.path.join(get_data_path(), "*.json"))
    for filepath in filepaths:
        slide_file_list.append(os.path.basename(filepath))
    for Reader in get_reader_classes():
        slide_reader_list.append(Reader.get_reader_name())
    os.makedirs(os.path.join(get_data_path(), "results"), exist_ok=True)
    with open(os.path.join(get_data_path(), "results", "index.json"), "w") as f:
        json.dump(
            {
                "slide_reader_list": slide_reader_list,
                "slide_file_list": slide_file_list,
            },
            f,
        )


def save_versions(versions):
    with open(os.path.join(get_data_path(), "results", "versions.json"), "w") as f:
        json.dump(
            versions,
            f,
        )


def download_slides_and_test_slide_readers():
    create_index()
    filepaths = glob.glob(os.path.join(get_data_path(), "*.json"))
    for filepath in filepaths:
        with open(filepath) as f:
            slide_file = SlideFile(**json.load(f))
        if "TESTDATA_PATH" in os.environ and "CI" in os.environ:
            download_folder = os.path.join(
                "/testdata",
                "wsi_file_format_and_performance_test",
                "slides",
            )
        else:
            download_folder = os.path.join(get_data_path(), "slides")
        os.makedirs(download_folder, exist_ok=True)
        slide_filepath = slide_file.download(download_folder)
        versions = {}
        for Reader in get_reader_classes():
            file_extension = pathlib.Path(slide_filepath).suffix.replace(".", "")
            if file_extension in Reader.get_supported_formats():
                reader = Reader(slide_filepath)
                versions[Reader.get_reader_name()] = reader.get_version()
                print(
                    "Testing slide",
                    slide_file.name,
                    "with reader",
                    Reader.get_reader_name(),
                    "...",
                )
                result_file_path = test_slide_info(reader, slide_file, filepath)
                test_slide_data(reader, slide_file, result_file_path)
                test_performance(reader, result_file_path)
        save_versions(versions)


def main():
    download_slides_and_test_slide_readers()


if __name__ == "__main__":
    main()
